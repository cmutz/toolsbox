FROM alpine:latest
LABEL "author"="Clement Mutz"
LABEL "email"="<clement.mutz@ouest-france.fr>"
LABEL "github"="https://gitlab.com/cmutz/toolsbox"
LABEL "version"="1.4"
RUN echo "Install tools"
RUN apk add --no-cache lftp \
    curl \
    wget \
    git \
    bind-tools \
    iputils \
    postgresql16 \
    mariadb-client \
    bash \
    busybox-extras \
    openssh \
    openssh-keygen \
    jq

RUN ssh-keygen -f /root/.ssh/id_rsa -q -N '""'
RUN curl -Lo signmykey https://github.com/signmykeyio/signmykey/releases/download/v0.8.8/signmykey_linux_amd64
RUN chmod +x signmykey && mv signmykey /usr/bin/
RUN echo "user: clement.mutz" > ~/.signmykey.yml
